<?php
namespace ETM;

use Enpowi\App;
use RedBeanPHP\R;

class Address
{
  public $id;
  public $congregation;
  public $name;
  public $email;
  public $phone;
  public $address1;
  public $address2;
  public $city;
  public $state;
  public $zip;
  public $longitude;
  public $latitude;

  private $_bean;

  public function __construct($id = null, $bean = null) {
    $this->id = $id;

    if ($id !== null && $bean === null) {
      $bean = R::load('address', $id);
    }

    $this->_bean = $bean;
    $this->convertFromBean();
  }

  public static function byCongregation($congregation) {
    return array_map(function($bean) {
      return new Address($bean->id, $bean);
    }, R::find('address', ' congregation = :congregation ', ['congregation' => $congregation]));
  }

  public static function fromBean($bean) {
    return new self($bean->id, $bean);
  }

  private function convertFromBean() {
    $bean = $this->_bean;
    if ($bean === null) return $this;

    $this->id = $bean->id;
    $this->congregation = $bean->congregation;
    $this->name = $bean->name;
    $this->email = $bean->email;
    $this->phone = $bean->phone;
    $this->address1 = $bean->address1;
    $this->address2 = $bean->address2;
    $this->city = $bean->city;
    $this->state = $bean->state;
    $this->zip = $bean->zip;
    $this->longitude = $bean->longitude;
    $this->latitude = $bean->latitude;

    return $this;
  }

  public function replace() {
    $existingBean = R::load('address', $this->id);
    $sharedRecordList = null;
    if ($existingBean !== null) {
      $copy = R::dispense('addressedit');
      $copy->congregation = $existingBean->congregation;
      $copy->name = $existingBean->name;
      $copy->email = $existingBean->email;
      $copy->phone = $existingBean->phone;
      $copy->address1 = $existingBean->address1;
      $copy->address2 = $existingBean->address2;
      $copy->city = $existingBean->city;
      $copy->state = $existingBean->state;
      $copy->zip = $existingBean->zip;
      $copy->longitude = $existingBean->longitude;
      $copy->latitude = $existingBean->latitude;
      $copy->date = time();
      R::store($copy);
    }

    $bean = R::dispense('address');
    $bean->congregation = $this->congregation;
    $bean->name = $this->name;
    $bean->email = $this->email;
    $bean->phone = $this->phone;
    $bean->address1 = $this->address1;
    $bean->address2 = $this->address2;
    $bean->city = $this->city;
    $bean->state = $this->state;
    $bean->zip = $this->zip;
    $bean->longitude = $this->longitude ?: 0;
    $bean->latitude = $this->latitude ?: 0;
    $this->id = R::store($bean);

    $this->_bean = $bean;
    return $this;
  }

  public static function allAsGeoJson($congregation = null)
  {
    if ($congregation === null) {
      $beans = R::findAll('address');
    } else {
      $beans = R::findAll('address', ' congregation = :congregation ', ['congregation' => $congregation]);
    }
    $result = [];
    foreach ($beans as $bean) {
      if (empty($bean->longitude) || empty($bean->latitude)) continue;
      $result[] = [
        'type' => 'Feature',
        'geometry' => [
          'type' => 'Point',
          'coordinates' => [$bean->latitude, $bean->longitude]
        ],
        'properties' => [
          'id' => $bean->id,
          'phone' => $bean->phone,
          'name' => $bean->name,
          'address1' => $bean->address1,
          'address2' => $bean->address2,
          'city' => $bean->city,
          'zip' => $bean->zip
        ]
      ];
    }

    return [
      'type' => 'FeatureCollection',
      'features' => $result
    ];
  }

  public static function page($pageNumber = 1, $count = 25) {
    $pageNumber = max(1, $pageNumber);
    $number = (($pageNumber - 1) * $count) + 1;
    $beans = R::findAll('address', 'ORDER BY name LIMIT :initialRow, :limit', [
      ':limit' => $count,
      ':initialRow' => $number
    ]);
    $congregations = [];
    foreach($beans as $bean) {
      $congregations[] = new Address($bean->id, $bean);
    }
    return $congregations;
  }

  public static function all($congregation = null) {
    if ($congregation === null) {
      $beans = R::findAll('address');
    } else {
      $beans = R::findAll('address', ' congregation = :congregation', [
        'congregation' => $congregation
      ]);
    }
    $congregations = [];
    foreach($beans as $bean) {
      $congregations[] = new Address($bean->id, $bean);
    }
    return $congregations;
  }
}