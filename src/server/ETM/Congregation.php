<?php
namespace ETM;

use Enpowi\App;
use RedBeanPHP\R;

class Congregation
{
  public $number;
  public $name;
  public $address1;
  public $address2;
  public $city;
  public $state;
  public $zip;
  public $phone;
  public $id;

  private $_bean;

  public function __construct($number = null, $bean = null) {
    if ($bean !== null) {
      $this->_bean = $bean;
    } else if($number !== null) {
      $this->_bean = R::findOne('congregation', ' number = :number ', [ 'number'=> $number ]);
    }
    $this->convertFromBean();
  }

  private function convertFromBean() {
    $bean = $this->_bean;
    if ($bean === null) return $this;
    $this->number = $bean->number;
    $this->name = $bean->name;
    $this->address1 = $bean->address1;
    $this->address2 = $bean->address2;
    $this->city = $bean->city;
    $this->state = $bean->state;
    $this->zip = $bean->zip;
    $this->phone = $bean->phone;
    $this->id = $bean->id;

    return $this;
  }

  public static function count() {
    return R::count('congregation');
  }

  public static function pages($perPageCount = 25) {
    $count = self::count();
    $inPageCount = 0;
    $pages = [];
    $page = [];
    for ($i = 1; $i <= $count; $i++, $inPageCount++) {
      if ($inPageCount === $perPageCount) {
        $inPageCount = 0;
        $pages[] = $page;
        $page = [];
      }
      $page[] = $i;
    }
    $pages[] = $page;

    return $pages;
  }

  public static function page($pageNumber = 1, $count = 25) {
    $pageNumber = max(1, $pageNumber);
    $number = (($pageNumber - 1) * $count) + 1;
    $beans = R::findAll('congregation', 'ORDER BY number LIMIT :initialRow, :limit', [
      ':limit' => $count,
      ':initialRow' => $number
    ]);
    $congregations = [];
    foreach($beans as $bean) {
      $congregations[] = new Congregation($bean->number, $bean);
    }
    return $congregations;
  }

  public static function all() {
    $beans = R::findAll('congregation', ' order by number ');
    $congregations = [];
    foreach ($beans as $bean) {
      $congregations[] = new Congregation($bean->number, $bean);
    }

    return $congregations;
  }

  public function replace() {
    $user = App::user();
    $existingBeans = R::findAll('congregation', ' number = :number ', ['number' => $this->number]);
    $sharedRecordList = null;
    foreach ($existingBeans as $bean) {
      $copy = R::dispense('congregationedit');
      $copy->number = $bean->number;
      $copy->name = $bean->name;
      $copy->address1 = $bean->address1;
      $copy->address2 = $bean->address2;
      $copy->city = $bean->city;
      $copy->state = $bean->state;
      $copy->zip = $bean->zip;
      $copy->phone = $bean->phone;
      $copy->archived = time();
      $copy->archivedBy = $user->id;

      R::store($copy);
      R::trash($bean);
    }

    $bean = R::dispense('congregation');
    $bean->number = $this->number;
    $bean->name = $this->name;
    $bean->address1 = $this->address1;
    $bean->address2 = $this->address2;
    $bean->city = $this->city;
    $bean->state = $this->state;
    $bean->zip = $this->zip;
    $bean->phone = $this->phone;
    $bean->created = time();
    $bean->createdBy = $user->id;
    R::store($bean);

    $this->_bean = $bean;
    return $this;
  }
}