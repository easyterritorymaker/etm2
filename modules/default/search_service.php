<?php
use Enpowi\App;
use Enpowi\Modules\Module;
use ETM\Territory;
use ETM\Publisher;

Module::is();
$query = App::param('query');
$user = App::user();
$results = [];

if ($user->hasPerm('territory', 'view') && is_numeric($query)) {
  $results = array_map(function($territory) {
    return $territory->number;
  }, Territory::find($query));
} else if ($user->hasPerm('publisher', 'view')) {
  $results = array_map(function($publisher) {
    return $publisher->name;
  }, Publisher::searchByName($query));
}

echo json_encode($results);