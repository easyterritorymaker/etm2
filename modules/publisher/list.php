<?php
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;

Module::is();

(new DataOut)
  ->add('publishers', ETM\Publisher::all())
  ->add('action', '')
  ->add('pickedNames', [])
  ->bind();
?>
<form
    class="container"
    action="publisher/list_service"
    data-done="publisher/list"
    v-module>
  <title v-t>Publishers</title>
  <div
      id="menu"
      class="well well-sm"
      style="top: 0; margin: 0 -15px;"
      data-spy="affix">
    <select
      name="action"
      class="form-control inline"
      v-model="action">
      <option v-t value="">Action</option>
      <option v-t value="merge">Merge</option>
    </select>

    <select
      type="radio"
      name="publisher"
      class="form-control inline"
      v-show="pickedNames.length > 0 && action == 'merge'" >
      <option v-t value="">Into</option>
      <option v-for="pickedName in pickedNames" value="{{ pickedName }}">{{ pickedName }}</option>
    </select>

    <button v-t class="btn btn-success">Submit</button>
  </div>
  <table class="table">
    <thead>
    <tr>
      <th></th>
      <th v-t>Name</th>
      <th v-t>Commands</th>
    </tr>
    </thead>
    <tbody>
    <tr v-for=" publisher in publishers ">
      <td>
        <input type="checkbox" name="publishers[]" value="{{ publisher.name }}" v-model="pickedNames">
      </td>
      <td><a href="#/publisher/view?name={{ publisher.name }}">{{ publisher.name }}</a></td>
      <td><a href="#/territory/publisher?name={{ publisher.name }}">Territory History</a></td>
    </tr>
    </tbody>
  </table>
</form>
<script>
  var menu = app.getElementById('menu');
  $(menu).affix();

  app.oneTo().land(function() {
    //ensure that menu stays at whatever width when it goes
    menu.style.width = menu.parentNode.clientWidth + 'px';

    window.onresize = function() {
      menu.style.width = menu.parentNode.clientWidth + 'px';
    };
  });
</script>