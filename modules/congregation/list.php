<?php
use Enpowi\App;
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;
use ETM\Congregation;
Module::is();

$page = App::paramInt('page');

(new DataOut)
  ->add('congregationCount', Congregation::count())
  ->add('page', $page)
  ->add('congregations', Congregation::page($page))
  ->bind();
?>
<div v-module class="container">
<h3><span v-t>Congregations</span>
    <a
      v-title="New Congregation"
      v-show="hasPerm('congregation', 'edit')"
      href="#/congregation/edit"><span class="glyphicon glyphicon-plus-sign"></span></a></h3>
  <table class="table">
    <tr>
      <th v-t>Number</th>
      <th v-t>Name</th>
      <th></th>
    </tr>
    <tr v-for="congregation in congregations">
      <td>{{ congregation.number }}</td>
      <td>{{ congregation.name }}</td>
      <td v-show="hasPerm('congregation', 'edit')"><a href="#/congregation/view?number={{ congregation.number }}" class="btn btn-primary">View</a></td>
    </tr>
  </table>
</div>
