<?php

use Enpowi\App;
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;
use ETM\Congregation;
Module::is();

(new DataOut)
  ->add('congregation', new Congregation(App::param('number')))
  ->bind();
?>
<div
    v-module
    class="create container">
  <title>View Congregation {{ congregation.name}}</title>

  <h3>
    <span><span v-t>View Congregation: </span> {{ congregation.name }}</span>
    <a 
      v-show="hasPerm('congregation', 'edit')"
      href="#/congregation/edit?number={{ congregation.number }}"
      class="btn btn-primary pull-right" v-t>Edit</a>
  </h3>

  <table class="table">
    <tr>
      <th v-t>Number: </th>
      <td>{{ congregation.number }}</td>
    </tr>
    <tr>
      <th v-t>Name: </th>
      <td>{{ congregation.name }}</td>
    </tr>
    <tr>
      <th v-t>Address: </th>
      <td>
        {{ congregation.address1 }}<br />
        {{ congregation.address2 }}
      </td>
    </tr>
    <tr>
      <th v-t>City: </th>
      <td>{{ congregation.city }}</td>
    </tr>
    <tr>
      <th v-t>State: </th>
      <td>{{ congregation.state }}</td>
    </tr>
    <tr>
      <th v-t>Zip: </th>
      <td>{{ congregation.zip }}</td>
    </tr>
    <tr>
      <th v-t>Phone: </th>
      <td>{{ congregation.phone }}</td>
    </tr>
  </table>
</div>