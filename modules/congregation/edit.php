<?php

use Enpowi\App;
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;
use ETM\Congregation;
Module::is();

(new DataOut)
  ->add('congregation',
    App::paramIs('number')
      ? new Congregation(App::param('number'))
      : new Congregation()
  )
  ->bind();
?>
<form
    v-module
    action="congregation/edit_service"
    data-done="congregation/view?number={{ congregation.number }}"
    class="create container">
  <title v-t>Edit Congregation</title>

  <h3>
    <span><span v-t>Edit Congregation</span> {{ congregation.number }} {{ congregation.name }}</span>
    <a onclick="window.history.back()" class="btn btn-primary pull-right" v-t>Cancel</a>
    <button type="submit" class="btn btn-primary pull-right" v-t>Save</button>
  </h3>

  <div>
    <input type="hidden" value="{{ stringify(congregation) }}" name="congregation">
    <div class="form-group">
      <label v-t>Number</label>
      <input v-model="congregation.number" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>Name</label>
      <input v-model="congregation.name" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>Address 1</label>
      <input v-model="congregation.address1" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>Address 2</label>
      <input v-model="congregation.address2" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>City</label>
      <input v-model="congregation.city" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>State</label>
      <input v-model="congregation.state" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>Zip</label>
      <input v-model="congregation.zip" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>Phone</label>
      <input v-model="congregation.phone" class="form-control">
    </div>
  </div>
</form>