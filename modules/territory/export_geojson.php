<?php
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;

Module::is();

(new DataOut)
  ->add('congregationGeoJson', ETM\Territory::allGeoJson())
  ->bind();
?>
<div v-module>
    <h3>
        <span v-t>Export GeoJSON</span>
    </h3>
</div>
<script>
    const encodedGeoJson = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(data.congregationGeoJson));
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('href', encodedGeoJson);
    a.setAttribute('download', 'export.json');
    a.click();
    document.body.removeChild(a);
</script>