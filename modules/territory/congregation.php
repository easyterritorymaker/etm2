<?php
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;

Module::is();

(new DataOut)
  ->add('congregationGeoJson', ETM\Territory::allGeoJson())
  ->bind();
?>
<div>
  <style>
    .leaflet-label-overlay {
      color: #000000;
      font-weight: bold;
      size: 4em;
    }
  </style>
  <title v-t>Congregation Territory</title>
  <div id="map" style="width: 100%; min-height: 500px;"></div>
</div>
<script>
  var mapElement = app.getElementById('map');
  app.oneTo().land(function() {
    var map = L.map(mapElement),
        options = {
          style: {
            color: '#50B414',
            weight: 5,
            opacity: 0.65
          }
        },
        geoJsonLayer = L.geoJson(data.congregationGeoJson, options);

    console.log(geoJsonLayer);
    /*window.onresize = function() {
      mapElement.style.height = mapElement.parentNode.clientHeight = 'px';
    };
    window.onresize();*/
    map.fitBounds(geoJsonLayer.getBounds());

    L.tileLayer('http://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
      maxZoom: 18,
      attribution: '&nbsp;'
    }).addTo(map);

    geoJsonLayer.eachLayer(function(layer) {
      var label = layer.feature.properties.number,
          labelOverlay = new L.LabelOverlay(layer, label);

      map.addLayer(labelOverlay);
    }).addTo(map);
  });
</script>