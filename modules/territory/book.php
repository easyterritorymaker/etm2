<?php
use Enpowi\App;
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;
Module::is();

(new DataOut)
    ->add('territoryCount', ETM\Territory::count())
    ->add('pages', ETM\Territory::pages())
    ->bind();
?>
<div class="container">
  <h3 v-t>Territory Book</h3>
  <title v-t>Territory Book</title>
  <table>
    <tr>
      <th>Page</th>
      <th>&nbsp;</th>
      <th>Territories</th>
    </tr>
    <tr v-for="page in pages">
      <td><a href="#/territory/page?page={{ $index + 1 }}">{{ $index + 1 }}</a></td>
      <td></td>
      <td>{{ page[0] + ' - ' + page[page.length - 1] }}</td>
    </tr>
  </table>
</div>