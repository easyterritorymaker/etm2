<?php
use Enpowi\App;
use Enpowi\Modules\Module;
Module::is();

$territory = new \ETM\Territory(App::param('number'));
$stop = false;
if (!$territory->exists()) {
  Module::paramRespond('numberResponse', 'Does not exist');
  $stop = true;
}

if (!$stop) {
  echo ETM\Record::checkIn($territory) === null ? -1 : 1;
}