<?php
use Enpowi\App;
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;
Module::is();

(new DataOut)
    ->add('publisher', (new \ETM\Publisher(App::param('name')))->bindRecords())
    ->bind();
?>
<div class="container">
  <title v-t>Territory History for {{ publisher.name }}</title>
  <h3><span v-t>Territory History for: </span><a href="#/publisher/view?name={{ publisher.name }}">{{ publisher.name }}</a></h3>
  <table class="table wide">
    <thead>
    <tr>
      <th v-t>Territory</th>
      <th v-t>Out</th>
      <th v-t>In</th>
    </tr>
    </thead>
    <tbody>
    <tr v-for="record in publisher.records">
      <td>
        <a href="#/territory/view?number={{ record.number }}">{{ record.number }}</a>
      </td>
      <td>{{ dateFormattedShort(record.out) }}</td>
      <td>{{ record.in ? dateFormattedShort(record.in) : '' }}</td>
    </tr>
    </tbody>
  </table>
</div>