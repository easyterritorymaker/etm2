<?php
use ETM\Territory;
use ETM\Publisher;
use ETM\Record;
use Enpowi\App;
use Enpowi\Modules\Module;
Module::is();

$number = App::param('number');
$publisherName = App::param('publisherName');
$create = App::param('create');
$update = App::paramBool('update');

$stop = false;
$territory = new Territory($number);
$publisher = new Publisher($publisherName);

if (strlen($publisherName) < 4) {
  Module::paramRespond('publisherNameResponse', 'Too short');
  $stop = true;
}

if (!$territory->exists()) {
  Module::paramRespond('numberResponse', 'Territory does not exist');
  $stop = true;
}

if (!$publisher->exists()) {
  if (!$create) {
    Module::paramRespond('create', false);
    Module::paramRespond('publisherExists', false);
    $stop = true;
  } else if (!$stop) {
    $publisher = Publisher::fromName($publisherName);
  }
} else if (!$update) {
  Module::paramRespond('publisherExists', true);
}

if (!$stop && $update) {
  if (!Record::checkOut($territory, $publisher)) {
    echo -1;
    die;
  }

  $territory
    ->bindGeoJson()
    ->bindRecords();

  Module::successRespond('showPublisherLookup', false);
  Module::successRespond('territory', $territory);
}