<?php
use Enpowi\Modules\Module;
use Enpowi\Modules\DataIn;
use ETM\Territory;
Module::is();

function toTerritory(Territory $t) { return $t; }

$territory = toTerritory((new DataIn)->in('territory'));

if (!is_string($territory->geoJson) || json_decode($territory->geoJson) === null) {
  throw new Exception('geojson in wrong format');
}

$territory->replace();

Module::successRespond('territoryUpdated', true);