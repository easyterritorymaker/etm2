<?php

use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;
Module::is();

(new DataOut)
    ->add('territories', ETM\Territory::page(1, 200))
    ->bind();
?>
<div v-module>
    <h3>
        <span v-t>Export CSV</span>
    </h3>
</div>
<script>
    const headers = 'Territory type	Territory number	Territory label	Territory notes	Publisher name	Email	Phone	Assigned	Returned	Notes'.split(/\t/g);
    const csv = [
        {
            type: headers[0],
            number: headers[1],
            label: headers[2],
            territryNotes: headers[3],
            publisherName: headers[4],
            email: headers[5],
            phone: headers[6],
            assigned: headers[7],
            returned: headers[8],
            publisherNotes: headers[9],
        }
    ];
    data.territories.forEach(territory => {
        territory.records.forEach(record => {
            csv.push({
                type: 'Valle Vista',
                number: territory.number,
                label: ' ',
                territryNotes: (territory.isOutOfCirculation ? 'Out of circulation due to opposition. ' : ' ') + (territory.notes || ' '),
                publisherName: record.publisher.name,
                email: record.publisher.email || ' ',
                phone: record.publisher.phone || ' ',
                assigned: record.out ? new Date(record.out * 1000).toLocaleDateString("en-US") : ' ',
                returned: record.in ? new Date(record.in * 1000).toLocaleDateString("en-US") : ' ',
                publisherNotes: ' '
            });
        });
    });

    JSONToCSVConvertor(csv, 'export');
    function JSONToCSVConvertor(arrData, fileName) {
        var CSV = '';    
        for (var index in arrData[0]) {
            CSV += arrData[0][index] + ',';
        }
        CSV += '\r\n';
    
        
        //1st loop is to extract each row
        for (var i = 1; i < arrData.length; i++) {
            var row = "";
            
            //2nd loop will extract each column and convert it in string comma-seprated
            for (var index in arrData[i]) {
                row += '"' + arrData[i][index] + '",';
            }

            row.slice(0, row.length - 1);
            
            //add a line break after each row
            CSV += row + '\r\n';
        }

        if (CSV == '') {        
            alert("Invalid data");
            return;
        } 
        
        //Initialize file format you want csv or xls
        var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
        
        // Now the little tricky part.
        // you can use either>> window.open(uri);
        // but this will not work in some browsers
        // or you will not get the correct file extension
        
        //this trick will generate a temp <a /> tag
        var link = document.createElement("a");
        link.href = uri;
        
        //set the visibility hidden so it will not effect on your web-layout
        link.style = "visibility:hidden";
        link.download = fileName + ".csv";
        
        //this part will append the anchor tag and remove it after automatic click
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
</script>