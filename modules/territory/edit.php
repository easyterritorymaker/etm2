<?php

use Enpowi\App;
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;
use ETM\Territory;

Module::is();
$territory = null;
$congregationGeoJson = null;

if (App::paramIs('number')) {
  $territory = new Territory(App::paramInt('number'));
} else {
  $territory = new Territory(Territory::highestNumber() + 1);
  $congregationGeoJson = ETM\Territory::allGeoJson();
}

$territory->bindGeoJson();

(new DataOut)
  ->add('territory', $territory)
  ->add('congregationGeoJson', $congregationGeoJson)
  ->bind();
?>
<form
    v-module
    action="territory/edit_service"
    data-done="territory/view?number={{ territory.number }}"
    class="create container">
  <style>
    .create #map {
      width: 100%;
      min-height: 500px;
    }
  </style>
  <title v-t>Edit Territory {{ territory.number }}</title>

  <h3>
    <span v-t>Edit Territory</span>
    <button type="submit" class="btn btn-primary pull-right" v-t>Save</button>
  </h3>

  <div>
    <input type="hidden" value="{{ stringify(territory) }}" name="territory">
    <div class="form-group">
      <label v-t>Number</label>
      <input v-model="territory.number" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>Locality</label>
      <input v-model="territory.locality" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>Congregation</label>
      <input v-model="territory.congregation" class="form-control" disabled="disabled">
    </div>
    <div class="form-group">
      <label v-t>Notes</label>
      <textarea v-model="territory.notes" class="form-control wide"></textarea>
    </div>
    <div class="form-group checkbox">
      <label>
        <input type="checkbox" v-model="territory.isOutOfCirculation">
        <span v-t> Out of circulation</span>
      </label>
    </div>
  </div>
  <hr>
  <div id="map"></div>
</form>
<link rel="stylesheet" href="vendor/leaflet-dist/leaflet.css">
<link rel="stylesheet" href="vendor/leaflet-draw/dist/leaflet.draw.css">
<script src="vendor/leaflet-dist/leaflet.js"></script>
<script src="vendor/leaflet-draw/dist/leaflet.draw.js"></script>
<script>
  var territory = data.territory,
      congregationGeoJson = data.congregationGeoJson,
      hasGeo = territory.geoJson ? true : false,
      mapElement = app.getElementById('map'),
      geoJson = hasGeo ? JSON.parse(territory.geoJson) : null;

  L.drawLocal.edit.toolbar.actions.save.text = 'Finish';
  L.drawLocal.edit.toolbar.actions.save.title = 'Finish editing, click save above to keep changes';

  app.oneTo().land(function() {
    var map = L.map(mapElement),
      options = {
        style: {
          color: '#50B414',
          weight: 5,
          opacity: 0.65
        }
      },
      mapGeoJson = hasGeo ? L.geoJson(geoJson, options) : L.geoJson(),
      drawControl = new L.Control.Draw({
        edit: {
          featureGroup: mapGeoJson
        }
      });

    map.addLayer(mapGeoJson);
    map.addControl(drawControl);

    if (hasGeo) {
      map.fitBounds(mapGeoJson.getBounds());
    } else {
      if (
        congregationGeoJson
        && congregationGeoJson.features
        && congregationGeoJson.features.length
      ) {
        var congLayer = L.geoJson(congregationGeoJson);
        map.addLayer(congLayer);
        map.fitBounds(congLayer.getBounds());
      } else {
        getLocation();
      }
    }

    map.setZoom(16);

    L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      attribution: '&nbsp;'
    }).addTo(map);

    map.on('draw:created', function (e) {
      mapGeoJson.addLayer(e.layer);
      data.territory.geoJson = JSON.stringify(mapGeoJson.toGeoJSON());
      console.log('created', mapGeoJson.toGeoJSON());
    });

    map.on('draw:edited', function (e) {
      data.territory.geoJson = JSON.stringify(mapGeoJson.toGeoJSON());
      console.log('edited', mapGeoJson.toGeoJSON());
    });

    function relocate(position) {
      map.setView([position.coords.latitude, position.coords.longitude], 13);
    }

    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(relocate);
      } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
      }
    }
  });
</script>