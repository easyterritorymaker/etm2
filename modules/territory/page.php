<?php
use Enpowi\App;
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;
Module::is();

$page = App::paramInt('page');

(new DataOut)
    ->add('territoryCount', ETM\Territory::count())
    ->add('page', $page)
    ->add('territories', ETM\Territory::page($page))
    ->bind();
?>
<div v-module>
  <title>Territory Assignment Records Page {{ page }}</title>
  <style>
    body > nav,
    body > aside,
    body > footer {
      display: none;
    }
  </style>
  <canvas id="page-detail" style="width: 2549px; height: 3301px;"></canvas>
</div>
<script>
  var pageDetail = app.getElementById('page-detail'),
    ctx = pageDetail.getContext('2d'),
    background = new Image();

  pageDetail.width = 2549;
  pageDetail.height = 3301;
  background.src = 'assets/s13.png';
  background.onload = function() {
    ctx.drawImage(background, 0, 0);

    data.territories.forEach(function(territory, territoryIndex) {
      var x = 140 + (456 * territoryIndex),
        y = 330;

      ctx.font = '36px monospace';
      ctx.fillText(territory.number, x + 85, y);

      territory.records.forEach(function(record, recordIndex) {
        var yDetail = y + 70 + (recordIndex * 113);
        if (record.publisher.name) {
          ctx.fillText(record.publisher.name, x, yDetail);
        }
        if (record.out) {
          ctx.fillText(moment(record.out * 1000).format('l'), x, yDetail + 55);
        }
        if (record.in) {
          ctx.fillText(moment(record.in * 1000).format('l'), x + 226, yDetail + 55);
        }
      });
    });
  };
</script>