<?php

use Enpowi\App;
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;
use ETM\Address;
Module::is();

(new DataOut)
  ->add('address', new Address(App::param('id')))
  ->bind();
?>
<div
    v-module
    class="create container">
  <title v-t>View Address</title>

  <h3>
    <span><span v-t>View Address: </span> {{ address.name }}</span>
    <a
      v-show=" hasPerm('address', 'edit') "
      href="/#/address/edit?id={{ address.id }}"
      class="btn btn-primary pull-right" v-t>Edit</a>
  </h3>

  <table class="table">
    <tr>
      <th v-t>Name: </th>
      <td>{{ address.name }}</td>
    </tr>
    <tr>
      <th v-t>Address: </th>
      <td>
        {{ address.address1 }}<br />
        {{ address.address2 }}
      </td>
    </tr>
    <tr>
      <th v-t>City: </th>
      <td>{{ address.city }}</td>
    </tr>
    <tr>
      <th v-t>State: </th>
      <td>{{ address.state }}</td>
    </tr>
    <tr>
      <th v-t>Zip: </th>
      <td>{{ address.zip }}</td>
    </tr>
    <tr>
      <th v-t>Phone: </th>
      <td>{{ address.phone }}</td>
    </tr>
    <tr>
      <th v-t>Email: </th>
      <td>{{ address.email }}</td>
    </tr>
  </table>
</div>