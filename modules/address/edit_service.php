<?php
use Enpowi\Modules\Module;
use Enpowi\Modules\DataIn;
use ETM\Address;
Module::is();

function toAddress(Address $a) { return $a; }

$address = toAddress((new DataIn)->in('address'))->replace();

Module::paramRespond('address', $address);