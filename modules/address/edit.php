<?php

use Enpowi\App;
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;
use ETM\Address;
Module::is();

(new DataOut)
  ->add('address',
    App::paramIs('id')
      ? new Address(App::param('id'))
      : new Address()
  )
  ->bind();
?>
<form
    v-module
    action="address/edit_service"
    data-done="address/view?id={{ address.id }}"
    class="create container">
  <title v-t>Edit Address</title>

  <h3>
    <span><span v-t>Edit Address</span> {{ address.name }}</span>
    <a onclick="window.history.back()" class="btn btn-primary pull-right" v-t>Cancel</a>
    <button type="submit" class="btn btn-primary pull-right" v-t>Save</button>
  </h3>

  <div>
    <input type="hidden" value="{{ stringify(address) }}" name="address">
    <div class="form-group">
      <label v-t>Name</label>
      <input v-model="address.name" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>Address 1</label>
      <input v-model="address.address1" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>Address 2</label>
      <input v-model="address.address2" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>City</label>
      <input v-model="address.city" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>State</label>
      <input v-model="address.state" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>Zip</label>
      <input v-model="address.zip" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>Phone</label>
      <input v-model="address.phone" class="form-control">
    </div>
    <div class="form-group">
      <label v-t>Email</label>
      <input v-model="address.email" class="form-control">
    </div>
  </div>
</form>