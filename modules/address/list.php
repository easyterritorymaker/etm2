<?php
use Enpowi\App;
use Enpowi\Modules\Module;
use Enpowi\Modules\DataOut;
use ETM\Address;
Module::is();

(new DataOut)
  ->add('addressesGeoJson', Address::allAsGeoJson())
  ->add('visibleFeatures', [])
  ->bind();
?>
<div class="container">
  <title v-t>Addresses</title>
  <h3><span v-t>Addresses</span>
    <a
      v-show=" hasPerm('address', 'edit') "
      v-title="New Address"
      href="#/address/edit"><span class="glyphicon glyphicon-plus-sign"></span></a></h3>
  <div
    id="map"
    class="pull-right well well-sm"
    data-spy="affix"
    v-show=" addressesGeoJson.features.length > 0 "
    style="width: 50%; top: 0; margin: 0 -15px;">&nbsp;</div>
  <table class="table" style="width: 50%;">
    <tr>
      <th v-t>Name</th>
      <th v-t>Address</th>
      <th v-t>Phone</th>
    </tr>
    <tr v-for="feature in visibleFeatures">
      <td><a
            href="#/address/view?id={{ feature.properties.id }}"
            v-title="View Address"
            target="_blank">{{ feature.properties.name || 'Unknown' }}</td>
      <td><a
          href="https://www.google.com/maps?q={{ feature.properties.address1 }}"
          target="_blank">{{ feature.properties.address1 }} {{ feature.properties.address2 }}</a></td>
      <td>{{ feature.properties.phone }}</td>
    </tr>
  </table>
</div>
<script>
  var mapElement = app.getElementById('map'),
    addresses = data.addressesGeoJson,
    visibleFeatures = data.visibleFeatures,
    allLayers = [],
    geoJson = new L.GeoJSON(addresses, {
      style: function (feature) {
        return {
          color: '#50B414',
          weight: 5,
          opacity: 0.65
        }
      },
      pointToLayer: function (feature, latlng) {
        var layer = L.circleMarker(latlng, {
          radius: 8,
          fillColor: "#ff7800",
          color: "#000",
          weight: 1,
          opacity: 1,
          fillOpacity: 0.8
        });
        allLayers.push(layer);
        return layer;
      }
    });
  mapElement.style.height = window.innerHeight + 'px';
  $(mapElement).affix();
  app.oneTo().land(function() {
    var map = L.map(mapElement);
    L.tileLayer('http://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
      maxZoom: 18,
      attribution: '&nbsp;'
    }).addTo(map);

    geoJson.addTo(map);

    map.fitBounds(geoJson.getBounds());
    updateBounds(map.getBounds());
    map
      .on('dragend', function() {
        updateBounds(map.getBounds());
      })
      .on('zoomend', function() {
        updateBounds(map.getBounds());
      });
  });

  function updateBounds(bounds) {
    while (visibleFeatures.length > 0) {
      visibleFeatures.pop();
    }
    allLayers.forEach(function(layer) {
      if (bounds.contains(layer._latlng)) {
        geoJson.addLayer(layer);
        visibleFeatures.push(layer.feature);
      } else {
        geoJson.removeLayer(layer);
      }
    });
  }

</script>