<?php
use ETM\Publisher;
use ETM\Record;
use ETM\Territory;

define('territory_simpleGeoJson1', '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[1,2],[1,2],[1,2],[1,2]]]}}]}');
define('territory_simpleGeoJson2', '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[2,1],[2,1],[2,1],[2,1]]]}}]}');

$tf->test('Territories can save', function(\Testify\Testify $tf) {
  //create initial territory
  $territory = new Territory(1);
  $territory->geoJson = territory_simpleGeoJson1;
  $territory->replace();

  //replace it
  $territory = new Territory(1);
  $territory->name = 'Macedonia';
  $territory->geoJson = territory_simpleGeoJson2;
  $territory->locality = 'Greece';
  $territory->congregation = 'Macedonia';
  $territory->notes = 'asdf asdf';
  $territory->isOutOfCirculation = false;

  $territory->replace();

  //assert the replaced overwrote the existing
  $territories = Territory::find(1);
  $tf->assertEquals(count($territories), 1);
  $territory = $territories[0];
  $territory->bindGeoJson();
  $tf->assertEquals($territory->name, 'Macedonia');
  $tf->assertEquals($territory->geoJson, territory_simpleGeoJson2);
  $tf->assertEquals($territory->locality, 'Greece');
  $tf->assertEquals($territory->congregation, 'Macedonia');
  $tf->assertEquals($territory->notes, 'asdf asdf');
  $tf->assertEquals($territory->isOutOfCirculation, false);
});

$tf->test('territories can run through data in and be replaced', function(\Testify\Testify $tf) {
  //create initial territory
  //replace it
  $territory = new Territory(1);
  $territory->name = 'Macedonia';
  $territory->geoJson = territory_simpleGeoJson1;
  $territory->locality = 'Greece';
  $territory->congregation = 'Macedonia';
  $territory->notes = 'asdf asdf';
  $territory->isOutOfCirculation = false;

  $territory->replace();
  $apostlePaul = Publisher::fromName('Apostle Paul');

  Record::checkOut($territory, $apostlePaul);
  Record::checkIn($territory);

  $serializer = new \mindplay\jsonfreeze\JsonSerializer();
  $serializer->skipPrivateProperties();
  $territory = $serializer->serialize($territory);
  function toTerritory(Territory $t) { return $t; }
  $territory = toTerritory($serializer->unserialize($territory));
  $tf->assertEquals($territory->name, 'Macedonia');
  $tf->assertEquals($territory->geoJson, territory_simpleGeoJson1);
  $tf->assertEquals($territory->locality, 'Greece');
  $tf->assertEquals($territory->congregation, 'Macedonia');
  $tf->assertEquals($territory->notes, 'asdf asdf');
  $tf->assertEquals($territory->isOutOfCirculation, false);
  $territory->replace();

  $territory = new Territory(1);
  $territory
    ->bindRecords()
    ->bindGeoJson();

  //assert the replaced overwrote the existing
  $tf->assertEquals($territory->name, 'Macedonia');
  $tf->assertEquals($territory->geoJson, territory_simpleGeoJson1);
  $tf->assertEquals($territory->locality, 'Greece');
  $tf->assertEquals($territory->congregation, 'Macedonia');
  $tf->assertEquals($territory->notes, 'asdf asdf');
  $tf->assertEquals($territory->isOutOfCirculation, false);

  function toRecord(Record $t) { return $t; }
  $tf->assertEquals(toRecord($territory->records[0])->publisher->name, 'Apostle Paul');
});
