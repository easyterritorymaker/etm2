<?php
use ETM\Publisher;
use ETM\Record;
use ETM\Territory;

define('record_simpleGeoJson', '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[1,2],[1,2],[1,2],[1,2]]]}}]}');

$tf->test('Record can check out', function(\Testify\Testify $tf) {
  $bobert = Publisher::fromName('Bobert');

  $territory = new Territory(1);
  $territory->geoJson = record_simpleGeoJson;
  $territory->replace();

  Record::checkOut($territory, $bobert);

  $tf->assertEquals((new Territory(1))->record->bean()->id, 1);
  $tf->assertEquals((new Territory(1))->status, 'out');
});

$tf->test('Record can check in', function(\Testify\Testify $tf) {
  $bobert = Publisher::fromName('Bobert');

  $territory = new Territory(1);
  $territory->geoJson = record_simpleGeoJson;
  $territory->replace();

  Record::checkOut($territory, $bobert);
  Record::checkIn($territory);
  $tf->assertEquals((new Territory(1))->status, 'in');
});

$tf->test('Record can not check in when already in', function(\Testify\Testify $tf) {
  $bobert = Publisher::fromName('Bobert');

  $territory = new Territory(1);
  $territory->geoJson = record_simpleGeoJson;
  $territory->replace();

  $tf->assertEquals(Record::checkIn($territory), null);
});

$tf->test('Record can not check out when already out', function(\Testify\Testify $tf) {
  $bobert = Publisher::fromName('Bobert');
  $robert = Publisher::fromName('Robert');

  $territory = new Territory(1);
  $territory->geoJson = record_simpleGeoJson;
  $territory->replace();

  Record::checkOut($territory, $bobert);
  $tf->assertEquals(Record::checkOut($territory, $robert), null);
});

$tf->test('Record updates territory when it is checked out', function(\Testify\Testify $tf) {
  $sam = Publisher::fromName('sam');

  // save to db first
  $territory = new Territory(12);
  $territory->geoJson = record_simpleGeoJson;
  $territory->replace();

  //recover from db
  $territory = new Territory(12);

  $tf->assert($territory->status === 'in');
  Record::checkOut($territory, $sam);
  $tf->assert($territory->status === 'out');
});