<?php
use ETM\Publisher;
use ETM\Record;
use ETM\Territory;

define('publisher_simpleGeoJson', '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[1,2],[1,2],[1,2],[1,2]]]}}]}');

$tf->test('Publisher can be merged', function(\Testify\Testify $tf) {
  $bobert = Publisher::fromName('Bobert');
  $bobby = Publisher::fromName('Bobby');
  $robert = Publisher::fromName('Robert');

  $territory1 = new Territory(1);
  $territory1->geoJson = publisher_simpleGeoJson;
  $territory1->replace();
  $territory2 = new Territory(2);
  $territory2->geoJson = publisher_simpleGeoJson;
  $territory2->replace();
  $territory3 = new Territory(3);
  $territory3->geoJson = publisher_simpleGeoJson;
  $territory3->replace();
  $territory4 = new Territory(4);
  $territory4->geoJson = publisher_simpleGeoJson;
  $territory4->replace();
  $territory5 = new Territory(5);
  $territory5->geoJson = publisher_simpleGeoJson;
  $territory5->replace();
  $territory6 = new Territory(6);
  $territory6->geoJson = publisher_simpleGeoJson;
  $territory6->replace();

  Record::checkOut($territory1, $bobert);
  Record::checkOut($territory2, $bobert);

  Record::checkOut($territory3, $bobby);
  Record::checkOut($territory4, $bobby);

  Record::checkOut($territory5, $robert);
  Record::checkOut($territory6, $robert);

  Publisher::merge('Bobert', ['Bobby', 'Robert']);

  $tf->assertEquals(count((new Publisher('Bobert'))->bindRecords()->records), 6);
});

$tf->test('Merging will not happen on destination publisher', function(\Testify\Testify $tf) {
  $bobert = Publisher::fromName('Bobert');
  $bobby = Publisher::fromName('Bobby');
  $robert = Publisher::fromName('Robert');

  $territory1 = new Territory(1);
  $territory1->geoJson = publisher_simpleGeoJson;
  $territory1->replace();
  $territory2 = new Territory(2);
  $territory2->geoJson = publisher_simpleGeoJson;
  $territory2->replace();
  $territory3 = new Territory(3);
  $territory3->geoJson = publisher_simpleGeoJson;
  $territory3->replace();
  $territory4 = new Territory(4);
  $territory4->geoJson = publisher_simpleGeoJson;
  $territory4->replace();
  $territory5 = new Territory(5);
  $territory5->geoJson = publisher_simpleGeoJson;
  $territory5->replace();
  $territory6 = new Territory(6);
  $territory6->geoJson = publisher_simpleGeoJson;
  $territory6->replace();

  Record::checkOut($territory1, $bobert);
  Record::checkOut($territory2, $bobert);

  Record::checkOut($territory3, $bobby);
  Record::checkOut($territory4, $bobby);

  Record::checkOut($territory5, $robert);
  Record::checkOut($territory6, $robert);

  Publisher::merge('Bobert', ['Bobert', 'Bobby', 'Robert']);

  $tf->assertEquals(count((new Publisher('Bobert'))->bindRecords()->records), 6);
});